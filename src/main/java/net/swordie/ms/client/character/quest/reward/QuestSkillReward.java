package net.swordie.ms.client.character.quest.reward;

import net.swordie.ms.client.character.Char;
import net.swordie.ms.client.character.skills.Skill;
import net.swordie.ms.loaders.DatSerializable;
import net.swordie.ms.loaders.SkillData;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

    public class QuestSkillReward implements QuestReward {
        private int id;
        private int skillLevel;
        private int masterLevel;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getSkillLevel() { return skillLevel; }

        public void setSkillLevel(int skillLevel) { this.skillLevel = skillLevel; }

        public int getMasterLevel() { return masterLevel; }

        public void setMasterLevel(int masterLevel) { this.masterLevel = masterLevel; }

        @Override
        public void giveReward(Char chr) {
            Skill skill = SkillData.getSkillDeepCopyById(getId());
            skill.setCurrentLevel(getSkillLevel());
            if (chr.getSkillLevel(skill) <= 0) {
                chr.addSkill(getId(), getSkillLevel(), getMasterLevel());
            } else {
                chr.chatMessage("You already have this skill.");
                chr.dispose();
            }
        }

        @Override
        public void write(DataOutputStream dos) throws IOException {
            dos.writeInt(getId());
            dos.writeInt(getSkillLevel());
            dos.writeInt(getMasterLevel());
        }

        @Override
        public DatSerializable load(DataInputStream dis) throws IOException {
            QuestSkillReward qsr = new QuestSkillReward();
            qsr.setId(dis.readInt());
            qsr.setSkillLevel(dis.readInt());
            qsr.setMasterLevel(dis.readInt());
            return qsr;
        }
    }
